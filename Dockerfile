FROM openjdk:11.0-jdk

COPY ./target/mars-mission-0.0.1-SNAPSHOT.jar /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch mars-mission-0.0.1-SNAPSHOT.jar'

ENTRYPOINT ["java","-jar","mars-mission-0.0.1-SNAPSHOT.jar"]
