# README #


__Start the service:__ 
```
mars-mission-svc$ docker-compose up --build
```
__To retrive all candidate info :__ 

```
localhost:8080/api/v1/candidate

```
__To retrive candidate info with filter :__

```
localhost:8080/api/v1/candidate?age=20-30
localhost:8080/api/v1/candidate?country=USA
localhost:8080/api/v1/candidate?fitness=fit
localhost:8080/api/v1/candidate?age=80-90&country=USA

```
### Assignment Overview ###

  A human mission to MARS is not very far, considering the technological advances humans have
  achieved in 21st century. Many scientists from across the globe are collectively working to
  shortlist candidates who are fit for such travel. Nasa in collaboration with global space agencies
  like ESA and ISRO have launched a website for everyone to apply. Shortlisted candidates will go
  through extensive training of 2 years. However contrary to expectations, millions have applied
  and people involved in shortlisting process are finding it difficult to process such large
  information and shortlist candidates.

### Requirements ###

* Design a REST API based microservice which can filter the data and provide result as
  json output.
* Only authenticated employees should be able to access these APIs. Every employee
  will have login and password created through registration. After login, same HTTP
  session can be used for further REST queries.
* You can design you own APIs based on the requirements presented. Should follow
  REST guidelines.
* Keep in mind that filters can be applied based on
  o A range (example AGE = between 30-50 years) or
  o Without range (example COUNTRY = India)
  o A combination of parameters like ( FITNETSS = “Fit” and AGE = between 30-50)
* Your REST API requests should accommodate all the above in its body.
* Use spring microservice framework and any NOSQL databases for your storage
  requirements.
* Design your project with MVC architecture in place.
* Ensure that enough logging is in place for troubleshooting purposes.
* Maturity of your solution will be judged based on object oriented and / or functional
  programming design and your ability to write clean, modular, extensible, maintainable,
  and testable code.
* Projects should be platform independent, and we should be able to deploy it using
  docker or any other containerization framework to any platform.
    