package com.nasa.marsmission.resource;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetCandidatesResponse {

	private List<Candidate> candidates;
	private Warning warningDetail;
}
