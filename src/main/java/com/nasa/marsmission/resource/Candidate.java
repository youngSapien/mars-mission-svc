package com.nasa.marsmission.resource;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder=true)
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Candidate {

	@Id
	private String id;
	private String name;
	private Integer age;
	private Float weight;
	private Float height;
	private String country;
	private Fitness fitness;
	private Exercise exercise;

	public Candidate(String name, Integer age, Float weight, String country, Float height, Fitness fitness, Exercise exercise) {
		this.name = name;
		this.age = age;
		this.weight = weight;
		this.height = height;
		this.country = country;
		this.fitness = fitness;
		this.exercise = exercise;
	}
}
