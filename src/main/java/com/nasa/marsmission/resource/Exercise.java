package com.nasa.marsmission.resource;

public enum Exercise {

	YES("Yes"),
	NO("No");
	
	private final String value;
	
	private Exercise(String value) {
		this.value = value;
	}
}
