package com.nasa.marsmission.resource;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder=true)
@AllArgsConstructor
@NoArgsConstructor
public class CandidateFilterCriteria {

	private String country;
	private Fitness fitness;
	private String ageRange;
	private Integer fromAge;
	private Integer toAge;
}
