package com.nasa.marsmission.resource;

public enum Fitness {
	FIT("FIT"),
	UNFIT("UNFIT");

	private final String value;
	
	private Fitness(String value) {
		this.value = value;
	}

}
