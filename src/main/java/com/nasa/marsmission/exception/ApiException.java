package com.nasa.marsmission.exception;

import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiException {
	
	private final String message;
	@JsonIgnore
	private final Throwable throwable;
	private final HttpStatus httpstatus;
	private final ZonedDateTime timestamp;
}
