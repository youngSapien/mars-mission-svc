package com.nasa.marsmission.exception;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<Object> handleBadRequestException(BadRequestException e) {
		
		ApiException exception = ApiException.builder()
			.message(e.getMessage())
			.throwable(e)
			.httpstatus(HttpStatus.BAD_REQUEST)
			.timestamp(ZonedDateTime.now(ZoneId.of("Z")))
			.build();
		
		return new ResponseEntity<>(exception, HttpStatus.BAD_REQUEST);
	}
}
