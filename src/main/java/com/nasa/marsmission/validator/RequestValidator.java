package com.nasa.marsmission.validator;

import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.nasa.marsmission.exception.BadRequestException;
import com.nasa.marsmission.resource.CandidateFilterCriteria;
import com.nasa.marsmission.resource.Fitness;

public class RequestValidator {
	
	@Autowired
    private static final Logger logger = LogManager.getLogger(RequestValidator.class);

	private static void validateGetCandidatesRequest(String country, String age, String fitness, CandidateFilterCriteria filterCriteria) {

		validateCountry(country, filterCriteria);
		validateAge(age, filterCriteria);
		validateFitness(fitness, filterCriteria);
		logger.info("getCandidates(): Request validated succesfully");
	}

	public static CandidateFilterCriteria populateCandidatesFilterCriteria(String country, String age, String fitness) {

		if (Objects.isNull(country)
				&& Objects.isNull(age)
				&& Objects.isNull(fitness))
			return null;

		CandidateFilterCriteria filterCriteria = new CandidateFilterCriteria();
		validateGetCandidatesRequest(country, age, fitness, filterCriteria);
		return filterCriteria;
	}

	private static void validateFitness(String fitness, CandidateFilterCriteria filterCriteria) {

		if (!Objects.isNull(fitness)) {
			try {
				if (Fitness.FIT.toString().equals(fitness.toUpperCase()))
					filterCriteria.setFitness(Fitness.FIT);
				else if (Fitness.UNFIT.toString().equals(fitness.toUpperCase()))
					filterCriteria.setFitness(Fitness.UNFIT);
				else
					throw new Exception("Invalid parameter for fitness");
			} catch (Exception ex) {
				logger.info("getCandidates(): Request validation failed");
				throw new BadRequestException(ex.getMessage());
			}
		}
	}

	private static void validateAge(String age, CandidateFilterCriteria filterCriteria) {

		if (!Objects.isNull(age)) {
			try {
				String[] ageRange = age.split("-");
				int fromAge, toAge;
				if (ageRange.length != 2) 
					throw new Exception("Invalid age parameter");

				fromAge = Integer.parseInt(ageRange[0]);
				toAge =Integer.parseInt(ageRange[1]);
				
				if (fromAge > toAge
						|| fromAge == toAge) {
					throw new Exception("Invalid age parameter range");
				}

				filterCriteria.setFromAge(Integer.parseInt(ageRange[0]));
				filterCriteria.setToAge(Integer.parseInt(ageRange[1]));
			} catch (NumberFormatException ex) {
				logger.info("getCandidates(): Request validation failed");
				throw new BadRequestException("Age range parameter should be numeric separated by '-'");
			} catch (Exception ex) {
				logger.info("getCandidates(): Request validation failed");
				throw new BadRequestException(ex.getMessage());
			}
		}
	}

	private static void validateCountry(String country, CandidateFilterCriteria filterCriteria) {

		if (!Objects.isNull(country))
			filterCriteria.setCountry(country);
	}
}
