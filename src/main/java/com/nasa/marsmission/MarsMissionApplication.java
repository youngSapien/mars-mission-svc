package com.nasa.marsmission;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsMissionApplication {

	@Autowired
    private static final Logger logger = LogManager.getLogger(MarsMissionApplication.class);

    public static void main(String[] args) {
		SpringApplication.run(MarsMissionApplication.class, args);
		logger.info("Application started successfully");
	}
}
