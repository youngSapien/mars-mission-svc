package com.nasa.marsmission.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.nasa.marsmission.MarsMissionApplication;
import com.nasa.marsmission.repository.CandidateRepository;
import com.nasa.marsmission.resource.Candidate;
import com.nasa.marsmission.resource.CandidateFilterCriteria;
import com.nasa.marsmission.resource.GetCandidatesResponse;
import com.nasa.marsmission.resource.Warning;
import com.nasa.marsmission.utils.ErrorBuilder;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class CandidateService {

	@Autowired
	private final CandidateRepository candidateRepository;
	@Autowired
	private final MongoTemplate template;
	@Autowired
    private static final Logger logger = LogManager.getLogger(CandidateService.class);

	public GetCandidatesResponse getCandidates(CandidateFilterCriteria filterCriteria) {

		GetCandidatesResponse getCandidatesResponse = new GetCandidatesResponse();
		List<Candidate> candidates = new ArrayList<>();

		try {

			logger.info("getCandidates(): Retrieving candidate information");
			candidates = retrieveCandidates(filterCriteria);
			
			if (candidates.size() == 0) {
				throw new Exception("No record found for the given criteria");
			}
			logger.info("getCandidates(): Retrieved candidate successfully");
 		} catch(Exception e) {

 			logger.warn("getCandidates(): Retrieving candidate information failed");
			Warning warning = ErrorBuilder.populateWarningDetail(e, HttpStatus.INTERNAL_SERVER_ERROR);
			getCandidatesResponse.setWarningDetail(warning);
		} finally {
			getCandidatesResponse.setCandidates(candidates);
		}
		return getCandidatesResponse;
	}

	private List<Candidate> retrieveCandidates(CandidateFilterCriteria candidateCriteria) {

		if (null == candidateCriteria)
			return candidateRepository.findAll();
		else
			return retriveCandidatesByFilter(candidateCriteria);
	}

	private List<Candidate> retriveCandidatesByFilter(CandidateFilterCriteria candidateCriteria) {

		Query query = new Query();
		if (null != candidateCriteria.getFromAge() && null != candidateCriteria.getToAge()) 	{
			query.addCriteria(new Criteria().andOperator(
					Criteria.where("age").gte(candidateCriteria.getFromAge()),
					Criteria.where("age").lte(candidateCriteria.getToAge())));
		}
		if (null != candidateCriteria.getCountry() && !candidateCriteria.getCountry().isEmpty()) {
			query.addCriteria(Criteria.where("country").is(candidateCriteria.getCountry()));
		}
		if (null != candidateCriteria.getFitness()) {
			query.addCriteria(Criteria.where("fitness").is(candidateCriteria.getFitness()));
		}
		return template.find(query, Candidate.class);
	}
}
