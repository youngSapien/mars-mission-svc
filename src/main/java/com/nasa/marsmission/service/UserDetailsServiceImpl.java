package com.nasa.marsmission.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.nasa.marsmission.resource.User;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private final MongoTemplate template;
	@Autowired
    private static final Logger logger = LogManager.getLogger(UserDetailsServiceImpl.class);

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		

		logger.info("authentication(): Fetching user information");
		Query query = new Query();
		query.addCriteria(Criteria.where("username").is(username));
		List<User> user = template.find(query, User.class);
		
		if (user.size() < 1) {
			logger.warn("authentication(): Invalid username");
			throw new UsernameNotFoundException("User not found");
		}
		return new org.springframework.security.core.userdetails.User(user.get(0).getUsername(), user.get(0).getPassword(), new ArrayList<>());
	}
}
