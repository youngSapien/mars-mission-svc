package com.nasa.marsmission.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nasa.marsmission.resource.User;

public interface UserRepository extends MongoRepository<User, String>{

}
