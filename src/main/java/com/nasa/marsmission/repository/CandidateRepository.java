package com.nasa.marsmission.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nasa.marsmission.resource.Candidate;

public interface CandidateRepository extends MongoRepository<Candidate, String>{
	
	List<Candidate> findByAgeBetween(int from, int to);
	List<Candidate> findByCountry(String country);
	List<Candidate> findByFitness(String fitness);
}
