package com.nasa.marsmission.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nasa.marsmission.resource.CandidateFilterCriteria;
import com.nasa.marsmission.resource.GetCandidatesResponse;
import com.nasa.marsmission.service.CandidateService;
import com.nasa.marsmission.validator.RequestValidator;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(("api/v1/candidate"))
@AllArgsConstructor
public class CandidateController {

	@Autowired
	private final CandidateService candidateService;
	@Autowired
    private static final Logger logger = LogManager.getLogger(CandidateController.class);

    @GetMapping
	public ResponseEntity<GetCandidatesResponse> getCandidates(
			@RequestParam(value = "country", required=false) String country,
			@RequestParam(value = "age", required=false) String age,
			@RequestParam(value = "fitness", required=false) String fitness) {

		logger.info("getCandidates(): Operation Intatiated");
		CandidateFilterCriteria filterCriteria = RequestValidator.populateCandidatesFilterCriteria(country, age, fitness);
		GetCandidatesResponse getCandidatesResponse = candidateService.getCandidates(filterCriteria);
		return new ResponseEntity<GetCandidatesResponse>(getCandidatesResponse, HttpStatus.OK);
	}
}
