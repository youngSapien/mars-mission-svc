package com.nasa.marsmission.utils;

import org.springframework.http.HttpStatus;

import com.nasa.marsmission.resource.Warning;

public class ErrorBuilder {

	public static Warning populateWarningDetail(Exception e, HttpStatus status) {
		Warning warning = Warning.builder()
				.exception(e.getLocalizedMessage())
				.message(e.getMessage())
				.status(status)
				.build();
		return warning;
	}
}
